# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
#    * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    * See the License for the specific language governing permissions and
#    * limitations under the License.

# Built-in Imports
import testtools
import fabric
import yaml

# Third Party Imports

# Cloudify Imports is imported and used in operations
from kube_plugin import remote_tasks
from mock import patch, mock_open
# from cloudify import manager
from cloudify.state import current_ctx
from cloudify.mocks import MockContext
from cloudify.mocks import MockCloudifyContext
# from cloudify.mocks import MockNodeContext
# from cloudify.mocks import MockNodeInstanceContext
# from cloudify.mocks import MockRelationshipContext
# from cloudify.mocks import MockRelationshipSubjectContext
# from cloudify.exceptions import NonRecoverableError

_test_open = mock_open()


class rc():

    def __init__(self, retcode=0, stderr='this is some output'):
        self.return_code = retcode
        self.stderr = stderr


class TestKubeTasks(testtools.TestCase):

    def relationship_context(self):

        target_node_context = MockContext({
            'node': MockContext({
                'properties': {
                  'ip': "1.1.1.1",
                  'master_port': '33'
                }
            }),
            'instance': MockContext({
                 'runtime_properties': {}
            })
        })

        source_node_context = MockContext({
          'instance': MockContext({
             'runtime_properties': {}
          })
        })

        return MockCloudifyContext(
            node_id="test",
            source=source_node_context,
            target=target_node_context)

    def test_connect_local(self):
        """ This tests that connecting to a master node in local mode works """

        ctx = self.relationship_context()
        ctx._local = True
        current_ctx.set(ctx=ctx)

        remote_tasks.connect_master()
        self.assertIn('master_ip', ctx.source.instance.runtime_properties)
        self.assertEquals(ctx.source.instance.runtime_properties[
                          'master_ip'], "1.1.1.1")

    def test_connect_remote(self):
        """ This tests that connecting to a master node that cloudify isn't
            managing
        """
        ctx = self.relationship_context()
        ctx.target.node.type = "cloudify.kubernetes.Master"
        current_ctx.set(ctx=ctx)
        remote_tasks.connect_master()
        self.assertEquals(ctx.source.instance.runtime_properties[
                          'master_ip'], "1.1.1.1")
        self.assertEquals(
            ctx.source.instance.runtime_properties['master_port'], "33")

    def test_connect_proxy(self):
        """ This tests that connecting to a master node that cloudify
            isn't managing
        """
        ctx = self.relationship_context()
        ctx.target.node.type = "cloudify.nodes.DeploymentProxy"
        ctx.target.instance.runtime_properties['kubernetes_info'] = {}
        ctx.target.instance.runtime_properties[
            'kubernetes_info']['url'] = 'http://33.33.33.33:1234'
        current_ctx.set(ctx=ctx)
        remote_tasks.connect_master(
            kubernetes_url_prop='["kubernetes_info"]["url"]')

    @patch('kube_plugin.remote_tasks.run', spec=fabric.api.run)
    @patch('kube_plugin.remote_tasks.put', spec=fabric.api.put)
    @patch('kube_plugin.remote_tasks.open', _test_open)
    def test_kube_run_with_config(self, mock_put, mock_run):

        ctx = MockCloudifyContext(node_id='test',
                                  properties={
                                   'config': {'key1': {'key1.1': 'val1.1'}},
                                   'config_files': None,
                                   'ssh_username': 'ubuntu',
                                   'ssh_keyfilename': '/root/.ssh/agent_key',
                                     },
                                  runtime_properties={
                                   'master_ip': '1.1.1.1',
                                     }
                                  )
        current_ctx.set(ctx=ctx)
        mock_run.return_value = rc()
        remote_tasks.kube_run_expose()
        self.assertEquals(_test_open.call_count, 1)
        yamlout = ''
        for call in _test_open.mock_calls:
            if call[0] == '().write':
                yamlout += call[1][0]
        config_yaml = '{ "key1": { "key1.1": "val1.1" }}'
        y1 = yaml.load(config_yaml)
        y2 = yaml.load(yamlout)
        self.assertEquals(y1, y2)
        self.assertEquals(mock_put.call_count, 1)
        self.assertEquals(mock_run.call_count, 1)

    def test_kube_run_with_config_files_nosubs(self):

        @patch('kube_plugin.remote_tasks.open', _test_open)
        @patch('kube_plugin.remote_tasks.run', spec=fabric.api.run)
        @patch('kube_plugin.remote_tasks.put', spec=fabric.api.put)
        @patch('kube_plugin.remote_tasks.yaml.load', spec=yaml.load)
        def inner(self, mock_yaml, mock_put, mock_run):

            ctx = MockCloudifyContext(
                node_id='test',
                properties={'config': None,
                            'config_files': [{'file': 'test.yaml'}],
                            'ssh_username': 'ubuntu',
                            'ssh_keyfilename': '/root/.ssh/agent_key', },
                runtime_properties={'master_ip': '1.1.1.1', })

            _test_open.reset_mock()

            def dl(self):
                return "nopath"
            ctx.download_resource = dl

            mock_run.return_value = rc()
            mock_yaml.return_value = {
                'kind': 'pod', 'metadata': {
                    'name': 'service'}}

            current_ctx.set(ctx=ctx)
            remote_tasks.kube_run_expose()

            # reads then writes
            self.assertEquals(_test_open.call_count, 2)
            self.assertEquals(mock_put.call_count, 1)
            self.assertEquals(mock_run.call_count, 1)

            yamlout = ''
            for call in _test_open.mock_calls:
                if call[0] == '().write':
                    yamlout += call[1][0]
            return yamlout

        yamlout = inner(self)
        config_yaml = "{'kind':'pod','metadata':{'name':'service'}}"
        y1 = yaml.load(config_yaml)
        y2 = yaml.load(yamlout)
        self.assertEquals(y1, y2)

    def test_kube_run_with_config_files_nosubs_multi(self):

        @patch('kube_plugin.remote_tasks.open', _test_open)
        @patch('kube_plugin.remote_tasks.run', spec=fabric.api.run)
        @patch('kube_plugin.remote_tasks.put', spec=fabric.api.put)
        @patch('kube_plugin.remote_tasks.yaml.load', spec=yaml.load)
        def inner(self, mock_yaml, mock_put, mock_run):

            ctx = MockCloudifyContext(node_id='test',
                                      properties={
                                       'config': None,
                                       'config_files': [{'file': 'test.yaml'},
                                                        {'file': 'test2.yaml'}
                                                        ],
                                       'ssh_username': 'ubuntu',
                                       'ssh_keyfilename':
                                       '/root/.ssh/agent_key',
                                         },
                                      runtime_properties={
                                       'master_ip': '1.1.1.1',
                                         }
                                      )

            _test_open.reset_mock()

            def dl(self):
                return "nopath"
            ctx.download_resource = dl

            mock_run.return_value = rc()
            mock_yaml.return_value = {
                'kind': 'pod', 'metadata': {
                    'name': 'service'}}

            current_ctx.set(ctx=ctx)
            remote_tasks.kube_run_expose()

            # reads then writes
            self.assertEquals(_test_open.call_count, 4)
            self.assertEquals(mock_put.call_count, 2)
            self.assertEquals(mock_run.call_count, 2)

            yamlout = ''
            for call in _test_open.mock_calls:
                if call[0] == '().write':
                    yamlout += call[1][0]
            return yamlout

        yamlout = inner(self)
        config_yaml = "{'kind':'pod','metadata':{'name':'service'}}"
        y1 = yaml.load(config_yaml)
        y2 = yaml.load(yamlout)
        self.assertEquals(y1, y2)

    def test_kube_run_with_config_file_subs(self):

        @patch('kube_plugin.remote_tasks.open', _test_open)
        @patch('kube_plugin.remote_tasks.manager.get_rest_client')
        @patch('kube_plugin.remote_tasks.run', spec=fabric.api.run)
        @patch('kube_plugin.remote_tasks.put', spec=fabric.api.put)
        @patch('kube_plugin.remote_tasks.yaml.load', spec=yaml.load)
        def inner(self, mock_yaml, mock_put, mock_run, rest_open):

            # simple, runtime prop, and context prop
            overrides = [
              "['key1']='sub1'",
              "['key2']='@{test,master_ip}'",
              "['key3']='%{instance.id}'"
            ]
            ctx = MockCloudifyContext(node_id='test',
                                      properties={
                                       'config': None,
                                       'config_files': [{'file': 'test.yaml',
                                                         'overrides': overrides
                                                         }],
                                       'ssh_username': 'ubuntu',
                                       'ssh_keyfilename':
                                       '/root/.ssh/agent_key',
                                         },
                                      runtime_properties={
                                       'master_ip': '1.1.1.1',
                                         }
                                      )

            class mock_rest_client():

                class _instances():

                    def list(self, **kwargs):
                        return [
                                ctx._instance
                               ]
                node_instances = _instances()
            rest_open.return_value = mock_rest_client()

            _test_open.reset_mock()

            def dl(self):
                return "nopath"
            ctx.download_resource = dl

            mock_run.return_value = rc()
            mock_yaml.return_value = {
                'kind': 'pod',
                'metadata': {
                    'name': 'service'},
                'key1': 'val1',
                'key2': 'val2'}

            current_ctx.set(ctx=ctx)
            remote_tasks.kube_run_expose()

            # reads then writes
            self.assertEquals(_test_open.call_count, 2)
            self.assertEquals(mock_put.call_count, 1)
            self.assertEquals(mock_run.call_count, 1)

            yamlout = ''
            for call in _test_open.mock_calls:
                if call[0] == '().write':
                    yamlout += call[1][0]
            return yamlout

        yamlout = inner(self)
        config_yaml = "{'kind':'pod','metadata':{'name':'service'},\
                        'key1':'sub1','key2':'1.1.1.1','key3':'test'}"
        y1 = yaml.load(config_yaml)
        y2 = yaml.load(yamlout)
        self.assertEquals(y1, y2)
