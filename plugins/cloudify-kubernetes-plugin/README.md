## Cloudify Kubernetes Plugin

This project contains a plugin that enables Cloudify to install, configure, and run services on a Kubernetes cluster.

Limitations (as of 2/24/2017)
+ Tested on Cloudify 3.4.1

### Plugin components

#### cloudify.kubernetes.Master node type

Represents a Kubernetes master node.  This node should be used if Kubernetes was not started by Cloudify (and so a proxy can't be used).  It defines an `ip` and `master_port` property.

#### cloudify.kubernetes.MicroService type

Represents a "microservice" in a Kubernetes cluster.  Requires the [`cloudify.kubernetes.relationships.connected_to_master`](#conn-to-master) relationship to get connection information.  Can define a service by plugin properties, by embedded Kubernetes native YAML, and by referring to a standard Kubernetes YAML deployment manifest while permitting overrides.  When using either form of native YAML, the actual Kubernetes action performed is determined by the configuration, which means that in reality it may or may not actually create a Kubernetes service, replication control, or other artifact.  Actual command execution on Kubernetes is performed by the [fabric plugin](https://github.com/cloudify-cosmo/cloudify-fabric-plugin) by remotely executing the Kubernetes `kubectl` executable on the master node.

<b>Interesting properties</b>
<li> non-native service definition - uses kubectl on master to first run a "run" command, followed by an "expose" command.

 Property        | Description                                   
 --------------- |  ---------------------
 name            | service name                                  
 image           | image name                                    
 port            | service listening port                        
 target_port     | container port (default:port)                 
 protocol        | TCP/UDP  (default TCP)                        
 replicas        | number of replicas (default 1)                   
 run_overrides   | json overrides for kubectl "run"              
 expose_overrides| json overrides for kubectl "expose"          

<nbsp>
<li>native embedded properties

 Property        | Description                                 
 --------------- | ---------------------------------------------
 config          | indented children are native Kubernetes YAML

<nbsp>
<li>native file reference properties

 Property        | Description                                
 --------------- | ---------------------------------------------
 config_path     | path to Kubernetes manifest               
 config_overrides| replacement values for external manifest 

### Override Syntax
When configuring with external files, the files require no change to be used with Cloudify, but can be modified by means of "overrides", which can insert blueprint values dynamically.  The target file is parsed into a Python datastructure (dict of dicts and lists).  To understand how the substitutions work, consider this pod.yaml snippet:

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: nodecellar
spec:
  replicas: 2

```
Now imagine I wish for some reason to change the number of replicas to 3.  The "overrides" line in the blueprint would look like this:

`['spec']['replicas']=3`

Internally, the plugin simply evaluates this statement on the parsed data structure.  After all substitutions are done, a new `pod.yaml` is written to perform the actual deployment on the master node via `kubectl`.  The value type of the substitution line is a string, so standard intrinsics like `concat` and `get_property` can be used to insert properties from elsewhere in blueprints.

#### Special Substitution Syntax
Sometimes it is desirable to inject runtime properties or information from the cloudify context.  To enable this, the plugin implements a special syntax.

##### Runtime Properties @{}
To insert runtime properties as values of substitutions, use the `@{}` syntax.  It takes two arguments; a node name and a property name.  For example, if I need to inject a dynamically discovered port from another node, you could use something like `[some][path]=@{target_node,discovered_port}`.

##### Cloudify Context %{}
To insert values from the cloudify [context](http://cloudify-plugins-common.readthedocs.io/en/3.3/context.html), use the `%{}` syntax.  It takes a single argument; a path in the Cloudify node context object.  For example, to insert the node id of the service, you could use something like `[some][path]=${node.id}`.  This is equivalent to evaluating `ctx.node.id` in plugin code.



#### cloudify.kubernetes.relationships.connected_to_master relationship <a id="#conn-to-master"></a>

Just retrieves the master ip and port for use by the dependent node.

#### "Generic" Workflows

With the exception of the `kube_scale` workflow (covered below), these workflows just delegate to `kubectl` on the master.  They all share a parameter called `master`.  The `master` parameter is set to the node name of the Kubernetes master that the workflow is to be run against.  Another pattern is to provide many (but not all) of the parameters that `kubectl` accepts, but using the `overrides` property as a catch all.
These workflows are provided as samples.  It should be understood that any actual producion blueprint would only implement workflows relevant to the blueprint purpose, which may or may not include the following, and probably contain others.

Workflow name| Description
------ | -------
kube_run         | `kubectl run` equivalent
kube_expose      | `kubectl run` equivalent
kube_stop        | `kubectl stop` equivalent
kube_delete      | `kubectl delete` equivalent

#### The "kube_scale" Workflows

The function of `kube_scale` is not to scale Kubernetes minion/node servers.  Scaling Kubernetes is handled by the standard `scale` workflow in Cloudify.  `kube_scale` scales deployed Microservices.  The amount of scale can be supplied as either an fixed number (e.g. 5) or an increment (e.g. +2 or -1).  The parameters are as follows:

Parameter | Description
------- | --------
master             | the master node in the blueprint.  Can be a standard node or a deployment proxy.
ssh_user           | the user that Kubernetes was installed as
ssh_keyfilename    | the key file name for the ssh_user
name               | the name of the Microservice node
amount             | the scale value (e.g. 2), or the scale increment (e.g. "+1).
